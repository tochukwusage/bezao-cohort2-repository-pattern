﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Repositories;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Repositories;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using System.Text;

namespace CodeFirstSoln
{
    partial class Program
    {
        static void Main(string[] args)
        {
           EnrollUser();
            DepositMoney();
            WithdrawMoney();
            TransferFunds();
        }

        static void EnrollUser()
        {
            var pwHash = new PasswordHash();
            var salts = pwHash.GenerateSalt();
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Register(new RegisterViewModel{FirstName = "Junior", LastName = "Nwokolo", Email = "junior.sage@omekannaya.com", Username = "Sage",
                Birthday = new DateTime(2000, 01, 22), Password = pwHash.ComputeHash(Encoding.UTF8.GetBytes("1234@one"), Encoding.UTF8.GetBytes(salts)), 
                ConfirmPassword = pwHash.ComputeHash(Encoding.UTF8.GetBytes("1234@one"), Encoding.UTF8.GetBytes(salts))});           
        }

        static void DepositMoney()
        {
            ITransactionService transactionService = new TransactionService(new UnitOfWork(new BezaoPayContext()));
            transactionService.Deposit(new DepositViewModel { Amount = 1_000_001, RecipientAccountNumber = 1209374652 });
        }
        static void WithdrawMoney()
        {
            ITransactionService transactionService = new TransactionService(new UnitOfWork(new BezaoPayContext()));
            transactionService.Withdraw(new WithdrawalViewModel { Amount = 1_000_000, AccountNumber = 753485382 });
        }
        static void TransferFunds()
        {
            ITransactionService transactionService = new TransactionService(new UnitOfWork(new BezaoPayContext()));
            transactionService.Transfer(new TransferViewModel { SenderAccountNumber = 753485382, RecipientAccountNumber = 1209374652, Amount = 1_000_000 });
        }
    }
}
