﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.UnitOfWork;
using System.Linq;

namespace BEZAO_PayDAL.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TransactionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Deposit(DepositViewModel model)
        {
            var context = new BezaoPayContext();
            var transaction = new Transaction
            {
                Amount = model.Amount
            };
            var amount = transaction.Amount;
            var accountNumber = model.RecipientAccountNumber;
            var accountToFind = context.Accounts.Where(u => u.AccountNumber == accountNumber).FirstOrDefault();
            var oldBalance = accountToFind.Balance;
            var newBalance = oldBalance += amount;
            var account = new Account
            {
                AccountNumber = model.RecipientAccountNumber,
                Balance = newBalance
            };
            _unitOfWork.Accounts.Update(account);
            _unitOfWork.Commit();
            Console.WriteLine("Done!");
            
        }
        public void Withdraw(WithdrawalViewModel model)
        {
            var context = new BezaoPayContext();
            var transaction = new Transaction
            {
                Amount = model.Amount
            };

            var amount = transaction.Amount;
            var accountNumber = model.AccountNumber;
            var accountToFind = context.Accounts.Where(u => u.AccountNumber == accountNumber).FirstOrDefault();
            var oldBalance = accountToFind.Balance;
            var newBalance = oldBalance -= amount;

            var account = new Account
            {
                AccountNumber = model.AccountNumber,
                Balance = newBalance
            };
            _unitOfWork.Accounts.Update(account);
            _unitOfWork.Commit();
            Console.WriteLine("Done!");

        }
        public void Transfer(TransferViewModel model)
        {
            var context = new BezaoPayContext();
            var transaction = new Transaction
            {
                Amount = model.Amount
            };
            var amount = transaction.Amount;
            var senderAccountNumber = model.SenderAccountNumber;
            var recipientAccountNumber = model.RecipientAccountNumber;
            var senderAccountToFind = context.Accounts.Where(u => u.AccountNumber == senderAccountNumber).FirstOrDefault();
            var recipientAccountToFind = context.Accounts.Where(u => u.AccountNumber == recipientAccountNumber).FirstOrDefault();
            var oldSenderBalance = senderAccountToFind.Balance;
            var oldRecipientBalance = recipientAccountToFind.Balance;
            var newSenderBalance = oldSenderBalance -= amount;
            var newRecipientBalance = oldRecipientBalance += amount;

            var recipientAccount = new Account
            {
                AccountNumber = model.RecipientAccountNumber,
                Balance = newRecipientBalance
            };
            var senderAccount = new Account
            {
                AccountNumber = model.SenderAccountNumber,
                Balance = newSenderBalance
        };
            _unitOfWork.Accounts.Update(recipientAccount);
            _unitOfWork.Accounts.Update(senderAccount);
            _unitOfWork.Commit();
            Console.WriteLine("Done!");
        }
    }
}
